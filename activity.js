//Create a route /create to create a new course, save the course in DB and return the document
	//Only admin can create a course



//Create a route "/" to get all the courses, return all documents found
	//both admin and regular user can access this route



//Create a route "/:courseId" to retrieve a specific course, save the course in DB and return the document
	//both admin and regular user can access this route



//Create a route "/:courseId/update" to update course info, return the updated document
	//Only admin can update a course



//Create a route "/:courseId/archive" to update isActive to false, return updated document
	//Only admin can update a course



//Create a route "/:courseId/unArchive" to update isActive to true, return updated document
	//Only admin can update a course



//Create a route "/isActive" to get all active courses, return all documents found
	//both admin and regular user can access this route


//Create a route "/:id/delete-course" to delete a courses, return true if success
	//Only admin can delete a course
